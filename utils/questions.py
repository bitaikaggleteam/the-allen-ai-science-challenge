__author__ = 'cragall'
import csv
from enum import Enum

#Question = Enum("Question", "NoneT Default Gap Order End_sentence All_above Describes_best True_false_sentence")

class Question(Enum):
    NoneT   = 0
    Default = 1
    Gap     = 2
    Order   = 3
    End_sentence= 4
    All_above   = 5
    Describes_best = 6
    True_false_sentence = 7


def getQuestionsFromCSV(file):
    questions = []
    correctAnswers = []
    with open(file, "rb") as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter="\t")
        for row in csvreader:
            question = row["question"]
            q_answers = ((row["answerA"], row["answerB"], row["answerC"], row["answerD"]))
            if "correctAnswer" in row.keys():
                correctAnswers.append(row["correctAnswer"])
            questions.append((question, q_answers))
    return questions, correctAnswers

def getQuestionsTypes(questions):
    for question, answers in questions:
        print question, getQuestionType(question, answers)


def getQuestionType(question, answers):
    """
    :param question: question
    :type question: str
    :param answers: list of answers to the question
    :type answers: tuple or array of strings
    :return: type of question
    :type: Question(Enum)
    """
    typeq = [
        (_which_true_false_sentence, Question.True_false_sentence),
        (_which_describes_best, Question.Describes_best),
        (_all_of_the_above, Question.All_above),
        (_end_the_sentence, Question.End_sentence),
        (_order_question, Question.Order),
        (_gap_question, Question.Gap),
        (_default, Question.Default)
    ]

    for func, qType in typeq:
        if func(question, answers):
            return qType.value


def _default(question, answers):
    return True


def _gap_question(question, answers):
    """ Assumed that question contains '__' (two of '_')
        Example: "When light passes through the lens of your eye, it produces __ image on the retina." """
    if "__" in question:
        return True
    else:
        return False


def _order_question(question, answers):
    """Assumed that all answers contain '->' """
    answers_contains_order = map(lambda answer: "->" in answer, answers)
    return reduce(lambda x, y: x and y, answers_contains_order, True)


def _end_the_sentence(question, answers):
    """Assumed that this always ends with '.' and question don't end with '.' """
    answers_contains_order = map(lambda answer: answer[-1] == "." , answers)
    return reduce(lambda x, y: x and y, answers_contains_order, True) and question[-1] != "." and question[-1] != "?"


def _all_of_the_above(question, answers):
    """Assumed that one of the answers contains statment 'all of the above'"""
    answers_contains_order = map(lambda answer: answer == "all of the above", answers)
    return reduce(lambda x, y: x or y, answers_contains_order, False)


def _which_describes_best(question, answers):
    """ Assumed that question starts with 'Which of the following sentences' and doesn't contains TRUE or FALSE
    """
    return question.startswith("Which of the following sentences") and not ("TRUE" in question or "FALSE" in question)


def _which_true_false_sentence(question, answers):
    """ Assumed that question starts with 'Which of the following sentences' and contains contains TRUE or FALSE
        Example: Which of the following sentences
    """
    return question.startswith("Which of the following sentences") and ("TRUE" in question or "FALSE" in question)


# getQuestionsTypes(getQuestionsFromCSV("../training_set.tsv")[0][:100])
