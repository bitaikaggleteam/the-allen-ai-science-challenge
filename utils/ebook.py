import ebooklib
from ebooklib import epub
from bs4 import BeautifulSoup
import re


def read_epub(path, summary=False, paragraphs=True, divide_sentence=False):
    """
    file

    :return:List of paragraphs
    """
    # TODO: check if file exists
    if not path.endswith(".epub"):
        raise Exception("Wrong file type")
    else:
        output = []
        book = epub.read_epub(path)
        for item in book.get_items_of_type(ebooklib.ITEM_DOCUMENT):
            if re.match("\d+\.html", item.get_name()):
                print item.get_name()
                soup = BeautifulSoup(item.get_content(), 'html.parser')
                if summary:
                    ls = _get_summary(soup)
                    if ls:
                        output.append(ls)
                if paragraphs:
                    ls = _get_paragraphs(soup)
                    if ls:
                        output.append(ls)
        return output


def _get_summary(document):
    summary_points = []
    hs = document.find_all(['h2', 'h3'])
    for element in hs:
        if "Lesson Summary" == element.get_text().strip() or \
           "Summary" == element.get_text().strip():

            lst = element.find_next_sibling("ul")
            if lst:
                children = lst.find_all(['li'])
                for child in children:
                    text = child.get_text().strip()

                    # TODO: Add text processing - divide sentences or something else
                    summary_points.append(text)
    return summary_points


def _default_paragraph_filter(paragraph):
    if re.match("[\n\t ]*Figure", paragraph):
        return False
    if "http:" in paragraph:
        return False
    if "Click" in paragraph:
        return False

    return True


def _remove_spaces(paragraph):
    return re.sub("[\n\t ]+", ' ', paragraph, flags= re.UNICODE)


def _remove_figure(paragraph):
    """( Figure below )"""
    return re.sub(" ?\([ ]*Figure[\w ]*\)", "", paragraph)


def _default_paragraph_post_process(paragraph):
    paragraph = _remove_spaces(paragraph)
    paragraph = _remove_figure(paragraph)

    if paragraph and paragraph[0] == " ":
        paragraph = paragraph[1:]
    if paragraph and paragraph[-1] == " ":
        paragraph = paragraph[:-1]

    return paragraph


def _get_paragraphs(document, filter_func=_default_paragraph_filter, post_process_func=_default_paragraph_post_process):
    paragraphs = []
    p = document.find_all(["p"])
    for element in p:
        text = element.get_text().encode('utf-8')
        if filter_func(text):
            text = post_process_func(text)
            if not text:
                continue
            paragraphs.append(text)
    return paragraphs


# for i in read_epub("../test.epub"):
#     print i